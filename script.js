let toggleButtonId = document.getElementById('checkbox')
let basicId = document.getElementById('basicId')
let professionalId = document.getElementById('premiumId')
let masterId = document.getElementById('masterId')

function setPrices (isAnnual) {
  if (isAnnual) {
    basicId.textContent = '19.99'
    professionalId.textContent = '24.99'
    masterId.textContent = '39.99'
  } else {
    basicId.textContent = '199.99'
    professionalId.textContent = '249.99'
    masterId.textContent = '399.99'
  }
}

toggleButtonId.addEventListener('click', () => {
  setPrices(toggleButtonId.checked)
  localStorage.setItem('isAnnual', toggleButtonId.checked)
})

document.addEventListener('DOMContentLoaded', () => {
  const isAnnual = JSON.parse(localStorage.getItem('isAnnual')) || false
  toggleButtonId.checked = isAnnual
  setPrices(isAnnual)
})
